import 'package:flutter/material.dart';

void main() {
  runApp(const myfirstapplab());
}

class myfirstapplab extends StatelessWidget {
  const myfirstapplab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSU Trang',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon),
          title: Text('My first app'),
          actions: [
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.add_alarm)
            ),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.access_time)
            ),
          ],
        ),
      body: Center(
        child: Column(
          children: [
            // Image.asset('asset/1.jpg',
            // height: 250,
            // width: 200,
            // ),
            CircleAvatar(
              backgroundImage: AssetImage('asset/1.jpg',),
              radius: 150,
            ),
            Text(
              'Yonradee Suksai',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
    ),
    );
  }
}
